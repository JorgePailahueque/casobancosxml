/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CasoCajeros;

import java.util.ArrayList;

/**
 *
 * @author diego
 */
public class Cajero {
    private String clave;
    private ArrayList<Transaccion>transacciones=new ArrayList();

    public Cajero(String clave) {
        this.clave = clave;
    }

    public ArrayList<Transaccion> getTransacciones() {
        return transacciones;
    }

    public void setTransacciones(ArrayList<Transaccion> transacciones) {
        this.transacciones = transacciones;
    }
    
    public void addTransacciones(Transaccion x){
        transacciones.add(x);
    }
    public void mostrarOpciones(){}
    public void solicitarClave(){}
    public void VerificaBanco(){}
    public void darRespuesta(){}
    
    public String getClave() {
        return clave;
    }

    @Override
    public String toString() {
        return "Cajero{" + "clave :" + clave + ", transacciones :" + transacciones + '}';
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
    
    
}
