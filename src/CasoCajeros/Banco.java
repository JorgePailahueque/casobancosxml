/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CasoCajeros;

import java.util.ArrayList;

/**
 *
 * @author diego
 */
public class Banco {
    private int numBanco;
    private String nombre;
    private String direccion;
    private int telefono;
    private ArrayList<Cajero>cajeros = new ArrayList() ;
    private ArrayList<Cuenta>cuentas = new ArrayList();

    public Banco(int numBanco, String nombre, String direccion, int telefono) {
        this.numBanco = numBanco;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
    }
    
    public void addCuenta(Cuenta c){
        cuentas.add(c);
    }
    
    public void addCajero(Cajero c){
        cajeros.add(c);
    }
    public ArrayList<Cajero> getCajeros() {
        return cajeros;
    }

    public void setCajeros(ArrayList<Cajero> cajeros) {
        this.cajeros = cajeros;
    }

    public ArrayList<Cuenta> getCuentas() {
        return cuentas;
    }

    public void setCuentas(ArrayList<Cuenta> cuentas) {
        this.cuentas = cuentas;
    }
    public void verificarTransaccion(){}
    public void verificarCuenta(){}
    public void verificarSaldo(){}

    public int getNumBanco() {
        return numBanco;
    }

    public void setNumBanco(int numBanco) {
        this.numBanco = numBanco;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "Banco{" + "numBanco :" + numBanco + ", nombre :" + nombre + ", direccion :" + direccion + ", telefono :" + telefono + ", cajeros :" + cajeros + ", cuentas :" + cuentas + '}';
    }
    
    
}
