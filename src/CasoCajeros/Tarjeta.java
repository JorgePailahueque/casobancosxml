/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CasoCajeros;

/**
 *
 * @author diego
 */
public class Tarjeta {
    private int numCuenta;
    private int numtarjeta;
    private int numCliente;

    public Tarjeta(int numCuenta, int numtarjeta, int numCliente) {
        this.numCuenta = numCuenta;
        this.numtarjeta = numtarjeta;
        this.numCliente = numCliente;
    }

    public int getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(int numCuenta) {
        this.numCuenta = numCuenta;
    }

    public int getNumtarjeta() {
        return numtarjeta;
    }

    public void setNumtarjeta(int numtarjeta) {
        this.numtarjeta = numtarjeta;
    }

    public int getNumCliente() {
        return numCliente;
    }

    public void setNumCliente(int numCliente) {
        this.numCliente = numCliente;
    }

    @Override
    public String toString() {
        return "Tarjeta{" + "numCuenta :" + numCuenta + ", numtarjeta :" + numtarjeta + ", numCliente :" + numCliente + '}';
    }
    
}
