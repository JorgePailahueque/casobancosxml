/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CasoCajeros;
import com.google.gson.*;
import com.google.gson.stream.JsonWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
/**
 *
 * @author diego
 */
public class CasoCajeros {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParserConfigurationException, TransformerConfigurationException, TransformerException   {
        // TODO code application logic here
       
        Cliente c1 =new Cliente("juan","Temuco",1,194643452,82398283);
        Cliente c2 =new Cliente("Pedro","Temuco",2,196727823,89233490);
        Cliente c3 =new Cliente("Felipe","Victoria",3,189832983,78238458);
        Cliente c4 =new Cliente("Sara","Temuco",4,194233432,89123465);
        Cliente c5 =new Cliente("Daniela","Cocepcion",5,191234123,82137623);
        Cliente c6 =new Cliente("Pamela","Temuco",6,174245486,92389487);
        Cliente c7 =new Cliente("Luis","Lautaro",7,184259875,98712343);
        Cliente c8 =new Cliente("Carlos","Osorno",8,193534357,981238345);
        Cliente c9 =new Cliente("Camilo","Temuco",9,198543984,87123454);
        Cliente c10 =new Cliente("Laura","Temuco",10,173458348,91908398);
        Cliente c11 =new Cliente("Noemi","Osorno",11,163447873,87124323);
        Cliente c12 =new Cliente("Pablo","Lautaro",12,138452348,91289289);
        Cliente c13 =new Cliente("Camila","Victoria",13,192347734,87329812);
        Cliente c14 =new Cliente("Loreto","Victoria",14,198234573,98237843);
        Cliente c15 =new Cliente("Ricardo","Temuco",15,183473563,78234312);
        Cliente c16 =new Cliente("Antonio","Victoria",16,193487478,89234365);
        Cliente c17 =new Cliente("Carolina","Osorno",17,182348977,98343545);
        Cliente c18 =new Cliente("Maria","Temuco",18,198238236,78238912);
        Cliente c19 =new Cliente("Catalina","Temuco",19,182374833,97859232);
        Cliente c20 =new Cliente("Denisse","Victoria",20,182348457,89234323);
        
        c1.addTarjeta(new Tarjeta(1001,1,1));
        c1.addTarjeta(new Tarjeta(1002,2,1));
        c1.addCuenta(new Cuenta(1001,1,"Ahorro",24,c1.getTarjetas().get(0)));
        c1.addCuenta(new Cuenta(1002,2,"Ahorro",24,c1.getTarjetas().get(1)));
        c1.getCuentas().get(0).addTransacciones(new Transaccion("giro", "giro", 10000));
        c1.getCuentas().get(1).addTransacciones(new Transaccion("giro", "giro", 20000));
        
        c2.addTarjeta(new Tarjeta(1003,3,2));
        c2.addTarjeta(new Tarjeta(1004,4,2));
        c2.addCuenta(new Cuenta(1003,2,"Ahorro",24,c2.getTarjetas().get(0)));
        c2.addCuenta(new Cuenta(1004,2,"Ahorro",24,c2.getTarjetas().get(1)));
        c2.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 30000));
        c2.getCuentas().get(1).addTransacciones(new Transaccion("giro", "d", 30000));
        
        c3.addTarjeta(new Tarjeta(1005,5,3));
        c3.addCuenta(new Cuenta(1005,5,"Ahorro",100000,c3.getTarjetas().get(0)));
        c3.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 30000));
        
        c4.addTarjeta(new Tarjeta(1006,6,4));
        c4.addTarjeta(new Tarjeta(1006,7,4));
        c4.addCuenta(new Cuenta(1006,4,"Ahorro",23,c4.getTarjetas().get(0)));
        c4.addCuenta(new Cuenta(1007,4,"Ahorro",23,c4.getTarjetas().get(1)));
        c4.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 30000));
        c4.getCuentas().get(1).addTransacciones(new Transaccion("giro", "d", 30000));
        
        c5.addTarjeta(new Tarjeta(1007,8,5));
        c5.addCuenta(new Cuenta(1007,5,"Ahorro",100000,c3.getTarjetas().get(0)));
        c5.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 20000));
       
        c6.addTarjeta(new Tarjeta(1008,9,6));
        c6.addCuenta(new Cuenta(1008,6,"Ahorro",100000,c3.getTarjetas().get(0)));
        c6.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 20000));
        
        c7.addTarjeta(new Tarjeta(1009,10,7));
        c7.addCuenta(new Cuenta(1009,7,"Ahorro",100000,c3.getTarjetas().get(0)));
        c7.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 20000));
        
        c8.addTarjeta(new Tarjeta(1010,11,8));
        c8.addCuenta(new Cuenta(1010,8,"Ahorro",100000,c3.getTarjetas().get(0)));
        c8.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 20000));
        
        c9.addTarjeta(new Tarjeta(1011,12,9));
        c9.addCuenta(new Cuenta(1011,9,"Ahorro",100000,c3.getTarjetas().get(0)));
        c9.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 20000));
          
        c10.addTarjeta(new Tarjeta(1012,13,10));
        c10.addCuenta(new Cuenta(1012,10,"Ahorro",100000,c3.getTarjetas().get(0)));
        c10.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 20000));
        
        c11.addTarjeta(new Tarjeta(1013,14,11));
        c11.addCuenta(new Cuenta(1011,13,"Ahorro",100000,c3.getTarjetas().get(0)));
        c11.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 20000));
        
        c12.addTarjeta(new Tarjeta(1014,15,12));
        c12.addCuenta(new Cuenta(1014,12,"Ahorro",100000,c3.getTarjetas().get(0)));
        c12.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 50000));
        
        c13.addTarjeta(new Tarjeta(1015,16,13));
        c13.addCuenta(new Cuenta(1015,13,"Ahorro",100000,c3.getTarjetas().get(0)));
        c13.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 50000));
        
        c14.addTarjeta(new Tarjeta(1016,17,14));
        c14.addCuenta(new Cuenta(1016,14,"Ahorro",100000,c3.getTarjetas().get(0)));
        c14.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 50000));
        
        c15.addTarjeta(new Tarjeta(1017,18,15));
        c15.addCuenta(new Cuenta(1017,15,"Ahorro",100000,c3.getTarjetas().get(0)));
        c15.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 50000));
        
        c16.addTarjeta(new Tarjeta(1018,19,16));
        c16.addCuenta(new Cuenta(1018,16,"Ahorro",100000,c3.getTarjetas().get(0)));
        c16.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 50000));
        
        c17.addTarjeta(new Tarjeta(1019,20,17));
        c17.addCuenta(new Cuenta(1019,17,"Ahorro",100000,c3.getTarjetas().get(0)));
        c17.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 50000));
        
        c18.addTarjeta(new Tarjeta(1020,21,18));
        c18.addCuenta(new Cuenta(1020,18,"Ahorro",100000,c3.getTarjetas().get(0)));
        c18.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 50000));
        
        c19.addTarjeta(new Tarjeta(1021,22,19));
        c19.addCuenta(new Cuenta(1021,19,"Ahorro",100000,c3.getTarjetas().get(0)));
        c19.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 30000));
        
        c20.addTarjeta(new Tarjeta(1022,23,20));
        c20.addCuenta(new Cuenta(1022,5,"Ahorro",100000,c3.getTarjetas().get(0)));
        c20.getCuentas().get(0).addTransacciones(new Transaccion("giro", "d", 40000));
        
        ArrayList<Cliente>clientes = new ArrayList();
        clientes.add(c1);
        clientes.add(c2);
        clientes.add(c3);
        clientes.add(c4);
        clientes.add(c5);
        clientes.add(c6);
        clientes.add(c7);
        clientes.add(c8);
        clientes.add(c9);
        clientes.add(c10);
        clientes.add(c11);
        clientes.add(c12);
        clientes.add(c13);
        clientes.add(c14);
        clientes.add(c15);
        clientes.add(c16);
        clientes.add(c17);
        clientes.add(c18);
        clientes.add(c19);
        clientes.add(c20);
       
        Banco b1 = new Banco(1, "Banco de Chile", "Temuco", 89123891);
        Banco b2 = new Banco(2, "Banco Estado", "Temuco", 62736771);
        Banco b3 = new Banco(3, "Banco Santander", "Temuco", 82892743);
        b1.addCajero(new Cajero("clave1"));
        b1.addCajero(new Cajero("clave2"));
        b2.addCajero(new Cajero("clave3"));
        b3.addCajero(new Cajero("clave4"));
        ArrayList<Banco>bancos = new ArrayList();
        bancos.add(b1);
        bancos.add(b2);
        bancos.add(b3);
        
        //Asignacion de cuentas de clientes a cuentas bancos. test
        for (int i = 0; i < clientes.size(); i++) {
            for (int j = 0; j < clientes.get(i).getCuentas().size(); j++) {
                if (i<10) {
                        b1.addCuenta(clientes.get(i).getCuentas().get(j));
                }else{
                    if (i<15 && i>9) {
                        b2.addCuenta(clientes.get(i).getCuentas().get(j)); 
                    }else{
                        b3.addCuenta(clientes.get(i).getCuentas().get(j));
                    }
                }
            }
        }
        //Asignacion transacciones de clientes a cajeros->transacciones
        for (int i = 0; i < clientes.size(); i++) {
            for (int j = 0; j < clientes.get(i).getCuentas().size(); j++) {
                for (int k = 0; k < clientes.get(i).getCuentas().get(j).getTransacciones().size(); k++) {
                    if (i<5) {
                        b1.getCajeros().get(0).addTransacciones(clientes.get(i).getCuentas().get(j).getTransacciones().get(k));
                    }else{
                        if (i<10) {
                            b1.getCajeros().get(1).addTransacciones(clientes.get(i).getCuentas().get(j).getTransacciones().get(k));
                        }else{
                            if (i<15 && i>9) {
                            b2.getCajeros().get(0).addTransacciones(clientes.get(i).getCuentas().get(j).getTransacciones().get(k));
                            }else{
                            b3.getCajeros().get(0).addTransacciones(clientes.get(i).getCuentas().get(j).getTransacciones().get(k));
                            }
                        }
                    }
                }
            }
        }
        
        DocumentBuilderFactory docFact= DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFact.newDocumentBuilder();
        Document doc = docBuilder.newDocument();
        
        Element rootElement = doc.createElement("Clientes");
        doc.appendChild(rootElement);
        
        for (int i = 0; i < clientes.size(); i++) {
            Element cli = doc.createElement("Cliente");
            rootElement.appendChild(cli);

            Element name= doc.createElement("nombre");
            name.appendChild(doc.createTextNode(clientes.get(i).getNombre()));
            cli.appendChild(name);

            Element direccion= doc.createElement("direccion");
            direccion.appendChild(doc.createTextNode(clientes.get(i).getDireccion()));
            cli.appendChild(direccion);


            Element numCliente= doc.createElement("numCliente");
            numCliente.appendChild(doc.createTextNode(""+clientes.get(i).getNumCliente()));
            cli.appendChild(numCliente);

            Element identificacion= doc.createElement("identificacion");
            identificacion.appendChild(doc.createTextNode(""+clientes.get(i).getIdentificacion()));
            cli.appendChild(identificacion);   

            Element telefono= doc.createElement("telefono");
            telefono.appendChild(doc.createTextNode(""+clientes.get(i).getTelefono()));
            cli.appendChild(telefono); 
            
            
            for (int j = 0; j < clientes.get(i).getCuentas().size(); j++) {
               
                Element cuenta = doc.createElement("cuenta");
                cli.appendChild(cuenta);
                
                Element numCuentaC = doc.createElement("numCuenta");
                numCuentaC.appendChild(doc.createTextNode(""+clientes.get(i).getCuentas().get(j).getNumCuenta()));
                cuenta.appendChild(numCuentaC);
                
                Element numClienteC = doc.createElement("numCliente");
                numClienteC.appendChild(doc.createTextNode(""+clientes.get(i).getCuentas().get(j).getNumCliente()));
                cuenta.appendChild(numClienteC);
                
                Element tipoDeCuenta = doc.createElement("tipoDeCuenta");
                tipoDeCuenta.appendChild(doc.createTextNode(""+clientes.get(i).getCuentas().get(j).getTipoDeCuenta()));
                cuenta.appendChild(tipoDeCuenta);
                
                Element saldo = doc.createElement("saldo");
                saldo.appendChild(doc.createTextNode(""+clientes.get(i).getCuentas().get(j).getSaldo()));
                cuenta.appendChild(saldo);
                
                Element tarjeta= doc.createElement("tarjeta");
                cuenta.appendChild(tarjeta);
                
                Element numCuenta= doc.createElement("numCuenta");
                numCuenta.appendChild(doc.createTextNode(""+clientes.get(i).getCuentas().get(j).getTarjeta().getNumCuenta()));
                tarjeta.appendChild(numCuenta);
                
                Element numTarjeta= doc.createElement("numTarjeta");
                numTarjeta.appendChild(doc.createTextNode(""+clientes.get(i).getCuentas().get(j).getTarjeta().getNumtarjeta()));
                tarjeta.appendChild(numTarjeta);
                
                Element numClienteT= doc.createElement("numClienteT");
                numClienteT.appendChild(doc.createTextNode(""+clientes.get(i).getCuentas().get(j).getTarjeta().getNumCliente()));
                tarjeta.appendChild(numClienteT);
                
               
            }
           
        }
         
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
	DOMSource source = new DOMSource(doc);
	StreamResult result = new StreamResult(new File("C:\\Users\\diego\\Documents\\NetBeansProjects\\CasoCajeros\\src\\CasoCajeros\\Clientes.xml"));
         
        transformer.transform(source, result);

	System.out.println("File saved!");
        
        Document doc2 = docBuilder.newDocument();
        Element raiz = doc2.createElement("Transacciones");
        doc2.appendChild(raiz);
        int cont=0;
        ArrayList<Object> tran = new ArrayList();
        for (int i = 0; i < clientes.size(); i++) {
            for (int j = 0; j < clientes.get(i).getCuentas().size(); j++) {
                for (int k = 0; k < clientes.get(i).getCuentas().get(j).getTransacciones().size(); k++) {
                    for (int l = 0; l < bancos.size(); l++) {
                        for (int m = 0; m < bancos.get(l).getCajeros().size(); m++) {
                            for (int n = 0; n < bancos.get(l).getCajeros().get(m).getTransacciones().size(); n++) {
                                if (clientes.get(i).getCuentas().get(j).getTransacciones().get(k).equals(bancos.get(l).getCajeros().get(m).getTransacciones().get(n))) {
                                    cont++;
                                    ArrayList<Object> s= new ArrayList();
                                    s.add(new Transaccion(bancos.get(l).getCajeros().get(m).getTransacciones().get(n).getDescripcin(), 
                                            bancos.get(l).getCajeros().get(m).getTransacciones().get(n).getTipo(), bancos.get(l).getCajeros().get(m).getTransacciones().get(n).getValor()));
                                    s.add(new Cajero(bancos.get(l).getCajeros().get(m).getClave()));
                                    s.add(new Cuenta(clientes.get(i).getCuentas().get(j).getNumCuenta(), clientes.get(i).getCuentas().get(j).getNumCliente(),
                                            clientes.get(i).getCuentas().get(j).getTipoDeCuenta(),clientes.get(i).getCuentas().get(j).getSaldo(), 
                                            clientes.get(i).getCuentas().get(j).getTarjeta()));
                                    s.add(new Cliente(clientes.get(i).getNombre(),clientes.get(i).getDireccion(), clientes.get(i).getNumCliente(), clientes.get(i).getIdentificacion(), clientes.get(i).getTelefono()));
                                    tran.add(s);
                                    Element trans =doc2.createElement("transaccion");
                                    raiz.appendChild(trans);

                                    Element des = doc2.createElement("descripcion");
                                    des.appendChild(doc2.createTextNode(bancos.get(l).getCajeros().get(m).getTransacciones().get(n).getDescripcin()));
                                    trans.appendChild(des);

                                    Element tipo = doc2.createElement("tipo");
                                    tipo.appendChild(doc2.createTextNode(bancos.get(l).getCajeros().get(m).getTransacciones().get(n).getTipo()));
                                    trans.appendChild(tipo);

                                    Element valor = doc2.createElement("valor");
                                    valor.appendChild(doc2.createTextNode(""+bancos.get(l).getCajeros().get(m).getTransacciones().get(n).getValor()));
                                    trans.appendChild(valor);
                                    
                                    Element cajero = doc2.createElement("Cajero");
                                    cajero.appendChild(doc2.createTextNode(bancos.get(l).getCajeros().get(m).getClave()));
                                    trans.appendChild(cajero);
                                    
                                    Element cuenta = doc2.createElement("Cuenta");
                                    trans.appendChild(cuenta);
                                    
                                    Element numCuenta = doc2.createElement("numCuenta");
                                    numCuenta.appendChild(doc2.createTextNode(""+clientes.get(i).getCuentas().get(j).getNumCuenta()));
                                    cuenta.appendChild(numCuenta);
                                    
                                    Element numCliente = doc2.createElement("numCliente");
                                    numCliente.appendChild(doc2.createTextNode(""+clientes.get(i).getCuentas().get(j).getNumCliente()));
                                    cuenta.appendChild(numCliente);
                                    
                                    Element tipoCuenta = doc2.createElement("tipoCuenta");
                                    tipoCuenta.appendChild(doc2.createTextNode(""+clientes.get(i).getCuentas().get(j).getTipoDeCuenta()));
                                    cuenta.appendChild(tipoCuenta);
                                    
                                    Element saldo = doc2.createElement("saldo");
                                    saldo.appendChild(doc2.createTextNode(""+clientes.get(i).getCuentas().get(j).getSaldo()));
                                    cuenta.appendChild(saldo);
                                    
                                    Element tarjeta = doc2.createElement("Tarjeta");
                                    trans.appendChild(tarjeta);
                                    
                                    Element numTarjeta = doc2.createElement("numTarjeta");
                                    numTarjeta.appendChild(doc2.createTextNode(""+clientes.get(i).getCuentas().get(j).getTarjeta().getNumtarjeta()));
                                    tarjeta.appendChild(numTarjeta);
                                    
                                    Element numCuentaT = doc2.createElement("numCuenta");
                                    numCuentaT.appendChild(doc2.createTextNode(""+clientes.get(i).getCuentas().get(j).getTarjeta().getNumtarjeta()));
                                    tarjeta.appendChild(numCuentaT);
                                    
                                    Element numClienteT = doc2.createElement("numClienteT");
                                    numClienteT.appendChild(doc2.createTextNode(""+clientes.get(i).getCuentas().get(j).getTarjeta().getNumtarjeta()));
                                    tarjeta.appendChild(numClienteT);
                                    
                                    Element cliente = doc2.createElement("Cliente");
                                    trans.appendChild(cliente);
                                    
                                    Element nombre = doc2.createElement("nombre");
                                    nombre.appendChild(doc2.createTextNode(clientes.get(i).getNombre()));
                                    cliente.appendChild(nombre);
                                    
                                    Element direccion = doc2.createElement("direccion");
                                    direccion.appendChild(doc2.createTextNode(clientes.get(i).getDireccion()));
                                    cliente.appendChild(direccion);
                                    
                                    Element numClienteC = doc2.createElement("numCliente");
                                    numClienteC.appendChild(doc2.createTextNode(""+clientes.get(i).getNumCliente()));
                                    cliente.appendChild(numClienteC);
                                    
                                    Element id = doc2.createElement("identificacion");
                                    id.appendChild(doc2.createTextNode(""+clientes.get(i).getIdentificacion()));
                                    cliente.appendChild(id);
                                    
                                    Element telefono = doc2.createElement("nombre");
                                    telefono.appendChild(doc2.createTextNode(clientes.get(i).getNombre()));
                                    cliente.appendChild(telefono);
                                    
                                    Element banco = doc2.createElement("Banco");
                                    trans.appendChild(banco);
                                    
                                    Element nombreB = doc2.createElement("nombre");
                                    nombreB.appendChild(doc2.createTextNode(bancos.get(l).getNombre()));
                                    banco.appendChild(nombreB);
                                    
                                    Element direccionB = doc2.createElement("direccion");
                                    direccionB.appendChild(doc2.createTextNode(bancos.get(l).getDireccion()));
                                    banco.appendChild(direccionB);
                                    
                                    Element numBanco = doc2.createElement("nnumBanco");
                                    numBanco.appendChild(doc2.createTextNode(""+bancos.get(l).getNumBanco()));
                                    banco.appendChild(nombreB);
                                    
                                    Element telefonoB = doc2.createElement("telefonoB");
                                    telefonoB.appendChild(doc2.createTextNode(""+bancos.get(l).getTelefono()));
                                    banco.appendChild(telefonoB);
                                }
                            }
                        }
                    }
                }
            }
        }
    	DOMSource sourc = new DOMSource(doc2);
    	StreamResult resul = new StreamResult(new File("C:\\Users\\diego\\Documents\\NetBeansProjects\\CasoCajeros\\src\\CasoCajeros\\Transacciones.xml"));
             
            transformer.transform(sourc, resul);
    
    	System.out.println("File saved!");
        
        Gson gson = new Gson();
        String json = gson.toJson(clientes.get(0).getCuentas().get(0));
        
        System.out.println(json);
       
        try{
            JsonWriter s= new JsonWriter(new FileWriter("C:\\Users\\diego\\Documents\\NetBeansProjects\\CasoCajeros\\src\\CasoCajeros\\JsonClientes.json"));
            s.beginObject();
            s.name("Clientes").jsonValue(gson.toJson(clientes));
            s.endObject();
            s.close();
         
            
        }catch(IOException e){
            e.printStackTrace();
        }
        
           try{
            JsonWriter s= new JsonWriter(new FileWriter("C:\\Users\\diego\\Documents\\NetBeansProjects\\CasoCajeros\\src\\CasoCajeros\\JsonTransacciones.json"));
            s.beginObject();
            s.name("Transacciones").jsonValue(gson.toJson(tran));
            s.endObject();
            s.close();
         
            
        }catch(IOException e){
            e.printStackTrace();
        }
            
    }
       
    
} 
