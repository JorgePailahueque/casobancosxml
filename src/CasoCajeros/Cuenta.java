/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CasoCajeros;

import java.util.ArrayList;

/**
 *
 * @author diego
 */
public class Cuenta {
    private int numCuenta;
    private int numCliente;
    private String TipoDeCuenta;
    private double Saldo;
    private ArrayList<Transaccion>transacciones = new ArrayList();
    private Tarjeta tarjeta;

    public Cuenta(int numCuenta, int numCliente, String TipoDeCuenta, double Saldo,
            Tarjeta tarjeta) {
        this.numCuenta = numCuenta;
        this.numCliente = numCliente;
        this.TipoDeCuenta = TipoDeCuenta;
        this.Saldo = Saldo;
        this.tarjeta = tarjeta;
    }

    public ArrayList<Transaccion> getTransacciones() {
        return transacciones;
    }

    public void setTransacciones(ArrayList<Transaccion> transacciones) {
        this.transacciones = transacciones;
    }
    
    public void addTransacciones(Transaccion t){
        transacciones.add(t);
    }
    
    public Tarjeta getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(Tarjeta tarjeta) {
        this.tarjeta = tarjeta;
    }
    public int getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(int numCuenta) {
        this.numCuenta = numCuenta;
    }

    public int getNumCliente() {
        return numCliente;
    }

    public void setNumCliente(int numCliente) {
        this.numCliente = numCliente;
    }

    public String getTipoDeCuenta() {
        return TipoDeCuenta;
    }

    public void setTipoDeCuenta(String TipoDeCuenta) {
        this.TipoDeCuenta = TipoDeCuenta;
    }

    public double getSaldo() {
        return Saldo;
    }

    public void setSaldo(double Saldo) {
        this.Saldo = Saldo;
    }

    @Override
    public String toString() {
        return "Cuenta{" + "numCuenta=" + numCuenta + ", numCliente=" + numCliente + ", TipoDeCuenta=" + TipoDeCuenta + ", Saldo=" + Saldo +  ", tarjeta=" + tarjeta + '}';
    }
    
    
}
