/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CasoCajeros;

/**
 *
 * @author diego
 */
public class Transaccion {
    private String descripcin;
    private String tipo;
    private double valor;

    public Transaccion(String descripcin, String tipo, double valor) {
        this.descripcin = descripcin;
        this.tipo = tipo;
        this.valor = valor;
    }
    
    public void registrar(){
    }
    
    public String getDescripcin() {
        return descripcin;
    }

    public void setDescripcin(String descripcin) {
        this.descripcin = descripcin;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getValor() {
        return (int)valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Transaccion{" + "descripcin :" + descripcin + ", tipo :" + tipo + ", valor :" + valor + '}';
    }
    
    
}
